package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class CelciusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int celcius = Celcius.fromFahrenheit(32);
		assertTrue("Invalid number of Fahrenheit", celcius == 0);
	}
	@Test
	public void testFromFahrenheitException() {
		int celcius = Celcius.fromFahrenheit(0);
		assertFalse("Invalid number of Fahrenheit", celcius == -18);
	}
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int celcius = Celcius.fromFahrenheit(-40);
		assertTrue("Unable to convert to Celcius", celcius == -40);
	}
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int celcius = Celcius.fromFahrenheit(-41);
		assertFalse("Invalid number of Fahrenheit", celcius == -40);
	}

}

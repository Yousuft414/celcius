package sheridan;

public class Celcius {
	
	public static int fromFahrenheit(int fahrenheit) {
		int celcius = (int)(Math.round((fahrenheit - 32) / 1.8));
		return celcius;
	}

}

